package main

import (
	"fmt"
	"math"
	"os"
	"time"
)

const (
	timeFormat = "2006-01-02"
)

func main() {
	firstDate, err := time.Parse(timeFormat, os.Args[1])
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	secondDate := time.Now()
	if len(os.Args) > 2 {
		secondDate, err = time.Parse(timeFormat, os.Args[2])
		if err != nil {
			fmt.Println(err)
			os.Exit(1)
		}
	}
	duration := secondDate.Sub(firstDate)
	dis := math.Ceil(duration.Hours() / 24)
	day := "day"
	if dis > 1 {
		day += "s"
	}
	fmt.Println(firstDate.Format(timeFormat), "and", secondDate.Format(timeFormat), "distances", dis, day, ".")
}
