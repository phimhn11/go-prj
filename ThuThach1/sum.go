package main

import (
	"fmt"
	"os"
	"regexp"
)

func main() {
	if len(os.Args) <= 2 {
		fmt.Println("Please give me two arguments!")
		os.Exit(1)
	}
	if check, _ := regexp.MatchString("^[0-9]+$", os.Args[1]); !check {
		fmt.Println("Invalid first argument!")
		os.Exit(1)
	}
	if check, _ := regexp.MatchString("^[0-9]+$", os.Args[2]); !check {
		fmt.Println("Invalid second argument!")
		os.Exit(1)
	}
	num1 := os.Args[1]
	num2 := os.Args[2]
	i := len(num1)
	j := len(num2)
	save := 0
	result := ""
	for i > 0 || j > 0 {
		sumDigit := 0
		if i > 0 {
			sumDigit += int(num1[i - 1]) - 48
		}
		if j > 0 {
			sumDigit += int(num2[j-1]) - 48
		}
		if save == 0 {
			fmt.Print("Lấy '", toStringNum(num1, i), "' cộng '", toStringNum(num2, j),
				"' => Kết quả được ", sumDigit, ", lưu ", sumDigit % 10)
			result = string(sumDigit % 10 + 48) + result
		} else {
			fmt.Print("Lấy '", toStringNum(num1, i), "' cộng '", toStringNum(num2, j),
				"' => Kết quả được ", sumDigit,
				", cộng với nhớ ", save, " được ", sumDigit + save, ", lưu ", (sumDigit + save) % 10)
			result = string((sumDigit + save) % 10 + 48) + result
		}
		save = (sumDigit + save) / 10
		if save > 0 {
			fmt.Println(", nhớ", save, ".")
		} else {
			fmt.Println(".")
		}
		i--
		j--
	}
	fmt.Println("Kết quả cuối cùng:", result)
}

func toStringNum(num string, i int) string {
	if i > 0 {
		return string(num[i-1])
	}
	return " ";
}
