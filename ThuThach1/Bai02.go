package main

import (
	"fmt"
	"os"
)

func main() {
	if len(os.Args) <= 1 {
		fmt.Println("Please give me one argument!")
		return
	}
	numbers := make(map[int]string)
	numbers[0] = "không"
	numbers[1] = "một"
	numbers[2] = "hai"
	numbers[3] = "ba"
	numbers[4] = "bốn"
	numbers[5] = "năm"
	numbers[6] = "sáu"
	numbers[7] = "bảy"
	numbers[8] = "tám"
	numbers[9] = "chín"
	s := os.Args[1]
	if len(s) == 3 {
		fmt.Print(numbers[int(s[0]-48)], " trăm ")
		numberToTextForDozens(1, s, numbers)
	} else if len(s) == 2 {
		numberToTextForDozens(0, s, numbers)
	} else {
		fmt.Print(numbers[int(s[0]-48)])
	}
}

func numberToTextForDozens(index int, s string, numbers map[int]string) {
	if s[index]-48 == 0 {
		if s[index+1]-48 != 0 {
			fmt.Print("lẻ ")
		}
	} else if s[index]-48 == 1 {
		fmt.Print("mười ")
	} else {
		fmt.Print(numbers[int(s[index]-48)], " mươi ")
	}
	if s[index+1]-48 != 0 {
		if int(s[index+1]-48) == 5 {
			fmt.Print("lăm")
		} else {
			fmt.Print(numbers[int(s[index+1]-48)])
		}
	}
}
