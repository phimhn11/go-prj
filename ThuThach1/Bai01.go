package main

import (
	"fmt"
	"math"
	"os"
	"strconv"
)

func main() {
	if len(os.Args) == 0 {
		fmt.Println("Please give me one argument!")
		return
	}
	var arguments []int
	for i := 0; i < len(os.Args); i++ {
		val, err := strconv.Atoi(os.Args[i])
		if err == nil {
			arguments = append(arguments, val)
		} else {
			fmt.Printf("Invalid value at %d: %s - %s\n",i, os.Args[i], err)
		}
	}
	min := 0
	max := 0
	median := 0
	sum := 0
	for i := 0; i < len(arguments); i++ {
		if (arguments[i] < arguments[min]) {
			min = i
		}
		if (arguments[i] > arguments[max]) {
			max = i
		}
		sum += arguments[i]
	}
	mid := (arguments[min] + arguments[max]) / 2
	gapMid := -1.0
	for i := 0; i < len(arguments); i++ {
		if gapMid == -1.0 || math.Abs(float64(arguments[i]-mid)) < gapMid {
			gapMid = math.Abs(float64(arguments[i]-mid))
			median = i
		}
	}
	fmt.Printf("Min: %d\nMax: %d\nMean: %f\n",
		arguments[min],
		arguments[max],
		float64(sum) / float64(len(arguments)))
	fmt.Print("Median: ")
	if len(arguments) % 2 == 0 {
		secondMid := 0
		for secondMid < len(arguments) && arguments[secondMid] < arguments[median] {
			secondMid++
		}
		fmt.Println(strconv.Itoa(arguments[median]) + " and " + strconv.Itoa(arguments[secondMid]));
	} else {
		fmt.Println(strconv.Itoa(arguments[median]));
	}
}
