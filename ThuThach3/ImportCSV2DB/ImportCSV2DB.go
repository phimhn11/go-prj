// ReadCSV_EmployeeV1.go project main.go
package main

import (
	"bufio"
	"database/sql"
	"fmt"
	_ "github.com/lib/pq"
	yaml "gopkg.in/yaml.v2"
	"io/ioutil"
	"log"
	"os"
	"strconv"
	"strings"
)

type DatasourceInfo struct {
	host			string 	`yaml:"host"`
	port			int		`yaml:"port"`
	user			string	`yaml:"user"`
	password		string	`yaml:"password"`
	databaseName	string	`yaml:"databaseName"`
}

type Employee struct {
	id            int
	name          string
	birthDay      string
	levelDegree   int
	levelEmployee float64
	gender        string
	description   string
}

func main() {
	args := os.Args
	if len(args) != 3 {
		fmt.Println("Invalid arguments")
		return
	}
	ds := ReadConfigFile(args[1])
	filePath := args[2]
	records, errors := ReadCsvFile(filePath)
	if len(errors) != 0 {
		fmt.Printf("CSV File %s has %d invalid employees\n\n", filePath, len(errors))
		PrintInvalidEmployees(errors, "invalidEmployees.csv")
	}
	for i := 0; i < len(records); i++ {
		UpsertEmployee(ds, records[i])
	}
	fmt.Println("Update employees to database successfully")
}

func PrintInvalidEmployees(errors []error, invalidFilePath string) {
	file, err := os.OpenFile(invalidFilePath, os.O_CREATE|os.O_WRONLY, 0600)
	if err != nil {
		panic(err)
	}

	defer file.Close()
	for i := 0; i < len(errors); i++ {
		fmt.Fprintf(file, "%s\n", errors[i])
	}
}

func ParseEmployee(emp string) (Employee, error) {
	//var employee Employee
	i := 0
	count := 0
	s := ""
	idStr := ""
	nameStr := ""
	birthDayStr := ""
	levelDegreeStr := ""
	levelEmployeeStr := ""
	genderStr := ""
	descriptionStr := ""
	for i < len(emp) {
		start := i
		end := i
		for end < len(emp) && (count == 6 || emp[end] != ',') {
			end++
		}
		s = s + emp[start:end]
		switch count {
		case 0:
			idStr = s
			break
		case 1:
			nameStr = s
			break
		case 2:
			birthDayStr = s
			break
		case 3:
			levelDegreeStr = s
			break
		case 4:
			levelEmployeeStr = s
			break
		case 5:
			genderStr = s
			break
		case 6:
			descriptionStr = s
			break
		}
		s = ""
		i = end + 1
		if count < 6 {
			count++
		}
	}
	id, _ := strconv.Atoi(idStr)
	name  := nameStr
	birthDay := birthDayStr
	levelDegree, _ := strconv.Atoi(levelDegreeStr)
	levelEmployee, _ := strconv.ParseFloat(levelEmployeeStr, 64)
	gender := genderStr
	description := strings.TrimSpace(descriptionStr)
	return Employee{
		id: id,
		name: name,
		birthDay: birthDay,
		levelDegree: levelDegree,
		levelEmployee: levelEmployee,
		gender: gender,
		description: description,
	}, nil
}

func ReadCsvFile(filePath string) ([]Employee, []error) {
	var employees []Employee
	var invalidEmployees []error
	f, err := os.Open(filePath)
	if err != nil {
		log.Fatal("Unable to read input file "+filePath, err)
	}
	defer f.Close()

	scanner := bufio.NewScanner(f)
	scanner.Scan()
	for scanner.Scan() {
		emp, err := ParseEmployee(scanner.Text())
		if err == nil {
			employees = append(employees, emp)
		} else {
			invalidEmployees = append(invalidEmployees, err)
		}
	}

	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}
	return employees, invalidEmployees
}

func UpsertEmployee(dataSource DatasourceInfo, emp Employee) {
	psqlInfo := fmt.Sprintf("host=%s port=%d user=%s "+
		"password=%s dbname=%s sslmode=disable",
		dataSource.host, dataSource.port, dataSource.user,
		dataSource.password, dataSource.databaseName)
	db, err := sql.Open("postgres", psqlInfo)
	if err != nil {
		panic(err)
	}
	defer db.Close()
	sqlStatement := `
		INSERT INTO employee(
			id, name, birthday, leveldegree, levelemployee, gender, description)
			VALUES ($1, $2, $3, $4, $5, $6, $7)
		ON CONFLICT (id) DO
		UPDATE
			SET name=$2, birthday=$3, leveldegree=$4, levelemployee=$5, gender=$6, description=$7
		RETURNING id`
	id := 0
	err = db.QueryRow(sqlStatement, emp.id, emp.name, emp.birthDay,
		emp.levelDegree, emp.levelEmployee, emp.gender, emp.description).Scan(&id)
	if err != nil {
		panic(err)
	}
	fmt.Println("Update record ID is:", id)
}

func ReadConfigFile(file string) DatasourceInfo {
	yfile, err := ioutil.ReadFile(file)

	if err != nil {

		log.Fatal(err)
	}

	data := make(map[string]string)
	err2 := yaml.Unmarshal(yfile, &data)

	if err2 != nil {

		log.Fatal(err2)
	}
	port, _ := strconv.Atoi(data["port"])
	return DatasourceInfo{
		host:         data["host"],
		port:         port,
		user:         data["user"],
		password:     data["password"],
		databaseName: data["databaseName"],
	}
}