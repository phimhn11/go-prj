package main

import (
	"database/sql"
	"fmt"
	_ "github.com/go-sql-driver/mysql"
	_ "github.com/lib/pq"
	yaml "gopkg.in/yaml.v2"
	"io/ioutil"
	"log"
	"os"
	"strconv"
	"strings"
	"time"
)

type DatasourceInfo struct {
	host			string 	`yaml:"host"`
	port			int		`yaml:"port"`
	user			string	`yaml:"user"`
	password		string	`yaml:"password"`
	databaseName	string	`yaml:"databaseName"`
}


type RawEmployee struct {
	id            int
	name          string
	birthDay      string
	levelDegree   int
	levelEmployee float64
	gender        string
	description   string
}


type Employee struct {
	id            int
	name          string
	birthDay      time.Time
	levelDegree   int
	levelEmployee float64
	gender        bool
	description   string
}


func main() {
	args := os.Args
	if len(args) != 3 {
		fmt.Println("Invalid arguments")
		return
	}
	postgresDatsource := ReadConfigFile(args[1])
	mysqlDatsource := ReadConfigFile(args[2])
	employees := FindEmployees(postgresDatsource)
	for _, rawEmployee := range employees {
		employee := ProcessEmployee(rawEmployee)
		UpsertEmployee(mysqlDatsource, employee)
	}
	fmt.Println("Transform data successfully")
}

func ProcessEmployee(rawEmployee RawEmployee) Employee {
	birthDay, err := ParseDate(rawEmployee.birthDay)
	if err != nil {
		fmt.Println("Invalid birthday in employee " + string(rawEmployee.id) + " birthday: " + rawEmployee.birthDay)
	}
	gender := ParseGender(rawEmployee.gender)
	return Employee{
		id:            rawEmployee.id,
		name:          rawEmployee.name,
		birthDay:      birthDay,
		levelDegree:   rawEmployee.levelDegree,
		levelEmployee: rawEmployee.levelEmployee,
		gender:        gender,
		description:   rawEmployee.description,
	}
}

func ParseGender(str string) (bool) {
	if strings.EqualFold(str, "true") ||
		strings.EqualFold(str, "1") ||
		strings.EqualFold(str, "nam") {
		return true
	}
	if strings.EqualFold(str, "false") ||
		strings.EqualFold(str, "0") ||
		strings.EqualFold(str, "nu") ||
		strings.EqualFold(str, "nữ") {
		return false
	}
	return false
}

func ParseDate(s string) (time.Time, error) {
	layout := "2/1/2006"
	d, err := time.Parse(layout, s)
	if err == nil {
		return d, nil
	}
	layout = "2006-1-2"
	d, err = time.Parse(layout, s)
	if err == nil {
		return d, nil
	}
	layout = "20060102"
	d, err = time.Parse(layout, s)
	if err == nil {
		return d, nil
	}
	return time.Time{}, err
}


func FindEmployees(datasource DatasourceInfo) []RawEmployee {
	// Query the DB
	datasourceInfo := fmt.Sprintf("host=%s port=%d user=%s "+
		"password=%s dbname=%s sslmode=disable",
		datasource.host, datasource.port, datasource.user,
		datasource.password, datasource.databaseName)
	db, err := sql.Open("postgres", datasourceInfo)
	if err != nil {
		panic(err)
	}
	defer db.Close()
	rows, err := db.Query(`SELECT id, name, birthday, leveldegree, levelemployee, gender, description	FROM employee`)
	if err != nil {
		log.Fatal(err)
	}
	defer rows.Close()

	var employees []RawEmployee
	for rows.Next() {
		var emp RawEmployee
		err := rows.Scan(&emp.id, &emp.name, &emp.birthDay, &emp.levelDegree, &emp.levelEmployee, &emp.gender, &emp.description)
		if err != nil {
			log.Fatal(err)
		}
		employees = append(employees, emp)
	}
	if err := rows.Err(); err != nil {
		log.Fatal(err)
	}
	return employees
}

func UpsertEmployee(dataSource DatasourceInfo, emp Employee) {
	connStr := dataSource.user + ":" + dataSource.password + "@tcp(" + dataSource.host + ":" + strconv.Itoa(dataSource.port) + ")/" + dataSource.databaseName
	db, err := sql.Open("mysql", connStr)
	if err != nil {
		panic(err)
	}
	defer db.Close()
	err = db.QueryRow("REPLACE INTO employee(id, name, birthday, leveldegree, levelemployee, gender, description) VALUES (?, ?, ?, ?, ?, ?, ?)",
		emp.id, emp.name, emp.birthDay, emp.levelDegree, emp.levelEmployee, emp.gender, emp.description).Err()
	if err != nil {
		panic(err)
	}
}

func ReadConfigFile(file string) DatasourceInfo {
	yfile, err := ioutil.ReadFile(file)

	if err != nil {

		log.Fatal(err)
	}

	data := make(map[string]string)
	err2 := yaml.Unmarshal(yfile, &data)

	if err2 != nil {

		log.Fatal(err2)
	}
	port, _ := strconv.Atoi(data["port"])
	return DatasourceInfo{
		host:         data["host"],
		port:         port,
		user:         data["user"],
		password:     data["password"],
		databaseName: data["databaseName"],
	}
}