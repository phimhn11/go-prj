package main

import (
	"bufio"
	"database/sql"
	"encoding/csv"
	"fmt"
	_ "github.com/go-sql-driver/mysql"
	yaml "gopkg.in/yaml.v2"
	"io/ioutil"
	"log"
	"os"
	"strconv"
	"strings"
	"time"
)

type DatasourceInfo struct {
	host			string 	`yaml:"host"`
	port			int		`yaml:"port"`
	user			string	`yaml:"user"`
	password		string	`yaml:"password"`
	databaseName	string	`yaml:"databaseName"`
}

type Employee struct {
	id            int
	name          string
	birthDay      time.Time
	levelDegree   int
	levelEmployee float64
	gender        bool
	description   string
	isDelete	  bool
}

func main() {
	args := os.Args
	if len(args) != 4 {
		fmt.Println("Invalid arguments")
		return
	}
	command := args[1]
	csvFilename := args[3]
	switch command {
	case "update":
		dataSource := ReadConfigFile(args[2])
		employees, _ := ReadCsvFile(args[3])
		UpdateEmployees(dataSource, employees)
		fmt.Println("Update employees successfully")
		break
	case "exportCsv":
		dataSource := ReadConfigFile(args[2])
		employees := FindEmployees(dataSource)
		WriteCSV(csvFilename, employees)
		fmt.Println("Export csv successfully")
	default:
		fmt.Println("Invalid command")
		break
	}
}


func FindEmployees(dataSource DatasourceInfo) []Employee {
	// Query the DB
	connStr := dataSource.user + ":" + dataSource.password + "@tcp(" + dataSource.host + ":" + strconv.Itoa(dataSource.port) + ")/" + dataSource.databaseName + "?parseTime=true"
	db, err := sql.Open("mysql", connStr)
	if err != nil {
		panic(err)
	}
	defer db.Close()
	rows, err := db.Query(`SELECT id, name, birthday, leveldegree, levelemployee, gender, description	FROM employee`)
	if err != nil {
		log.Fatal(err)
	}
	defer rows.Close()

	var employees []Employee
	for rows.Next() {
		var emp Employee
		err := rows.Scan(&emp.id, &emp.name, &emp.birthDay, &emp.levelDegree, &emp.levelEmployee, &emp.gender, &emp.description)
		if err != nil {
			log.Fatal(err)
		}
		employees = append(employees, emp)
	}
	if err := rows.Err(); err != nil {
		log.Fatal(err)
	}
	return employees
}

func WriteCSV(filename string, employees []Employee) {
	empData := [][]string{
		{"Id", "Name", "BirthDay", "LevelDegree", "LevelEmployee", "Gender", "Description"},
	}
	for _, emp := range employees {
		data := make([]string, 0)
		data = append(data, strconv.Itoa(emp.id))
		data = append(data, emp.name)
		data = append(data, emp.birthDay.Format("02/01/2006"))
		data = append(data, strconv.Itoa(emp.levelDegree))
		data = append(data, fmt.Sprintf("%.2f", emp.levelEmployee))
		data = append(data, fmt.Sprintf("%t", emp.gender))
		data = append(data, emp.description)
		empData = append(empData, data)
	}

	csvFile, err := os.Create(filename)

	if err != nil {
		log.Fatalf("failed creating file: %s", err)
	}

	csvWriter := csv.NewWriter(csvFile)

	for _, empRow := range empData {
		_ = csvWriter.Write(empRow)
	}
	csvWriter.Flush()
	csvFile.Close()

}

func UpdateEmployees(dataSource DatasourceInfo, employees []Employee) {
	for _, emp := range employees {
		if emp.isDelete {
			DeleteEmployee(dataSource, emp)
		} else {
			UpsertEmployee(dataSource, emp)
		}
	}
}

func DeleteEmployee(dataSource DatasourceInfo, emp Employee) {
	connStr := dataSource.user + ":" + dataSource.password + "@tcp(" + dataSource.host + ":" + strconv.Itoa(dataSource.port) + ")/" + dataSource.databaseName
	db, err := sql.Open("mysql",connStr)
	if err != nil {
		panic(err)
	}
	defer db.Close()
	err = db.QueryRow("DELETE FROM employee WHERE id = ?", emp.id).Err()
	if err != nil {
		panic(err)
	}
}

func UpsertEmployee(dataSource DatasourceInfo, emp Employee) {
	connStr := dataSource.user + ":" + dataSource.password + "@tcp(" + dataSource.host + ":" + strconv.Itoa(dataSource.port) + ")/" + dataSource.databaseName
	db, err := sql.Open("mysql", connStr)
	if err != nil {
		panic(err)
	}
	defer db.Close()
	err = db.QueryRow("UPDATE employee SET name = ?, birthDay = ?, leveldegree = ?, levelemployee = ?, gender = ?, description = ? WHERE id = ?",
		emp.name, emp.birthDay, emp.levelDegree, emp.levelEmployee, emp.gender, emp.description, emp.id).Err()
	if err != nil {
		panic(err)
	}
}

func ParseEmployee(emp string) (Employee, error) {
	//var employee Employee
	i := 0
	count := 0
	s := ""
	idStr := ""
	nameStr := ""
	birthDayStr := ""
	levelDegreeStr := ""
	levelEmployeeStr := ""
	genderStr := ""
	descriptionStr := ""
	for i < len(emp) {
		start := i
		end := i
		for end < len(emp) && (count == 6 || emp[end] != ',') {
			end++
		}
		s = s + emp[start:end]
		switch count {
		case 0:
			idStr = s
			break
		case 1:
			nameStr = s
			break
		case 2:
			birthDayStr = s
			break
		case 3:
			levelDegreeStr = s
			break
		case 4:
			levelEmployeeStr = s
			break
		case 5:
			genderStr = s
			break
		case 6:
			descriptionStr = s
			break
		}
		s = ""
		i = end + 1
		if count < 6 {
			count++
		}
	}
	id, _ := strconv.Atoi(idStr)
	if len(nameStr) == 0 &&
		len(nameStr) == 0 &&
		len(birthDayStr) == 0 &&
		len(levelDegreeStr) == 0 &&
		len(levelEmployeeStr) == 0 &&
		len(genderStr) == 0 &&
		len(descriptionStr) == 0 {
		return Employee{
			id: id,
			isDelete: true,
		}, nil
	}
	name  := nameStr
	birthDay, _ := ParseDate(birthDayStr)
	levelDegree, _ := strconv.Atoi(levelDegreeStr)
	levelEmployee, _ := strconv.ParseFloat(levelEmployeeStr, 64)
	gender := ParseGender(genderStr)
	description := strings.TrimSpace(descriptionStr)
	return Employee{
		id: id,
		name: name,
		birthDay: birthDay,
		levelDegree: levelDegree,
		levelEmployee: levelEmployee,
		gender: gender,
		description: description,
		isDelete: false,
	}, nil
}


func ReadCsvFile(filePath string) ([]Employee, []error) {
	var employees []Employee
	var invalidEmployees []error
	f, err := os.Open(filePath)
	if err != nil {
		log.Fatal("Unable to read input file "+filePath, err)
	}
	defer f.Close()

	scanner := bufio.NewScanner(f)
	scanner.Scan()
	for scanner.Scan() {
		emp, err := ParseEmployee(scanner.Text())
		if err == nil {
			employees = append(employees, emp)
		} else {
			invalidEmployees = append(invalidEmployees, err)
		}
	}

	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}
	return employees, invalidEmployees
}


func ParseGender(str string) (bool) {
	if strings.EqualFold(str, "true") ||
		strings.EqualFold(str, "1") ||
		strings.EqualFold(str, "nam") {
		return true
	}
	if strings.EqualFold(str, "false") ||
		strings.EqualFold(str, "0") ||
		strings.EqualFold(str, "nu") ||
		strings.EqualFold(str, "nữ") {
		return false
	}
	return false
}

func ParseDate(s string) (time.Time, error) {
	layout := "2/1/2006"
	d, err := time.Parse(layout, s)
	if err == nil {
		return d, nil
	}
	layout = "2006-1-2"
	d, err = time.Parse(layout, s)
	if err == nil {
		return d, nil
	}
	layout = "20060102"
	d, err = time.Parse(layout, s)
	if err == nil {
		return d, nil
	}
	return time.Time{}, err
}


func ReadConfigFile(file string) DatasourceInfo {
	yfile, err := ioutil.ReadFile(file)

	if err != nil {

		log.Fatal(err)
	}

	data := make(map[string]string)
	err2 := yaml.Unmarshal(yfile, &data)

	if err2 != nil {

		log.Fatal(err2)
	}
	port, _ := strconv.Atoi(data["port"])
	return DatasourceInfo{
		host:         data["host"],
		port:         port,
		user:         data["user"],
		password:     data["password"],
		databaseName: data["databaseName"],
	}
}

