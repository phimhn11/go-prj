## Thử thách chương 3

- Khởi tạo database với lệnh `docker-compose up -d` trong folder chưa docker-compose.yml
- Khởi tạo cấu trúc table của mysql và postgresql
>PostgreSQL
```
CREATE DATABASE db
    WITH 
    OWNER = root
    ENCODING = 'UTF8'
    LC_COLLATE = 'en_US.utf8'
    LC_CTYPE = 'en_US.utf8'
    TABLESPACE = pg_default
    CONNECTION LIMIT = -1;



-- Table: public.employee

-- DROP TABLE IF EXISTS public.employee;

CREATE TABLE IF NOT EXISTS public.employee
(
    id integer NOT NULL,
    name text COLLATE pg_catalog."default",
    leveldegree integer,
    levelemployee numeric,
    description text COLLATE pg_catalog."default",
    birthday text COLLATE pg_catalog."default",
    gender text COLLATE pg_catalog."default",
    CONSTRAINT employee_pkey PRIMARY KEY (id)
)

TABLESPACE pg_default;

ALTER TABLE IF EXISTS public.employee
    OWNER to root;

GRANT ALL ON TABLE public.employee TO root;
```

> MySQL
```create database import_db;
use import_db;
create table employee
(
    id            int        not null,
    name          text       null,
    birthday      date       null,
    leveldegree   int        null,
    levelemployee double     null,
    gender        tinyint(1) null,
    description   text       null,
    constraint employee_id_uindex
        unique (id)
);

alter table employee
    add primary key (id);
```

Tham số chương trình ImportCSV2DB, DataTransform và DataCRUDE
- ImportCSV2DB: ImportCSV2DB.exe `datasource yaml file` `datasource csv file`
- DataTransform: DataTransform.exe `datasource souce` `datasource souce destination`
- DataCRUDE: DataCRUDE.exe `command` `args`. `command` bao gồm: `update` và `exportCsv`
  > Update: DataCRUDE.exe update `datasource` `update csv file`.
  > Example: `DataCRUDE.exe update ./mysql-db.yaml ./new-employees.csv`
  
  > Update: DataCRUDE.exe exportCsv `datasource` `exported csv file path`.
  > Example: `DataCRUDE.exe exportCsv ./mysql-db.yaml ./export-employees.csv`

